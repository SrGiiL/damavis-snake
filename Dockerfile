FROM python:3.10.0rc2-alpine3.14

WORKDIR /usr/src/app

COPY . .

CMD [ "./index.py" ]
ENTRYPOINT [ "python3" ]
