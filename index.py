from itertools import product


def numberOfAvailableDifferentPaths(board, snake, depth):
    parametersValidator(board, snake, depth)

    possiblePaths = differentPossiblePaths(depth)
    
    paths = validPaths(board, snake, possiblePaths)

    return len(paths)


def parametersValidator(board, snake, depth):
    if len(board) != 2:
        exit("\n\nERROR: El tablero sólo puede contener dos elementos")
    if not 1 <= board[0] <= 10:
        exit("\n\nERROR: La cantidad de filas debe estar comprendida entre 1 y 10")
    if not 1 <= board[1] <= 10:
        exit("\n\nERROR: La cantidad de columnas debe estar comprendida entre 1 y 10")
    if not 3 <= len(snake) <= 7:
        exit("\n\nERROR: La serpiente tiene que tener una longitud de 3 a 7")
    for bodyPosition in snake:
        if len(bodyPosition) != 2:
            exit("\n\nERROR: Cuerpo de la serpiente erróneo")


def differentPossiblePaths(depth):

    availableMovements = ["L", "R", "D", "U"]
    dict_moves = {}

    for i in range(0, depth): dict_moves[i] = availableMovements

    paths = []
    for c in product(*[dict_moves[k] for k in sorted(dict_moves.keys())]):
        paths.append(''.join(c))

    return paths


def validPaths(board, snake, paths):

    validPaths = []
    for path in paths:
        copySnake = snake[:]
        valid = False
        for move in list(path):
            valid = validateMove(board, copySnake, move)
            if not valid:
                break
        if valid:
            validPaths.append(path)

    return validPaths


def validateMove(board, snake, move, true):

    newHead = snake[0][:]
    
    # Move head
    if move == "L": newHead[1] -= 1
    if move == "R": newHead[1] += 1
    if move == "D": newHead[0] += 1
    if move == "U": newHead[0] -= 1

    # Check that the snake does not leave the board
    if not 0 <= newHead[0] < board[0] or not 0 <= newHead[1] < board[1]: return False

    # Check if it is the position of the snake tail
    cola = False
    if snake[len(snake) -1] == newHead: cola = True

    # If not is the tail check if it is the snake body
    if not cola:
        for body in snake:
            if body[0] == newHead[0] and body[1] == newHead[1]:
                return False

    # Assign new position of the head
    snake.insert(0, newHead)

    # Drop the tail
    snake.pop(len(snake) - 1)

    return True


print("\nSNAKE 1:")
snake1 = [[2, 2], [3, 2], [3, 1], [3, 0], [2, 0], [1, 0], [0, 0]]
result = numberOfAvailableDifferentPaths([4, 3], snake1, 3)
print("RESULT: " + str(result) + "\n")

print("SNAKE 2:")
snake2 = [[0, 2], [0, 1], [0, 0], [1, 0], [1, 1], [1, 2]]
result = numberOfAvailableDifferentPaths([2, 3], snake2, 10)
print("RESULT: " + str(result) + "\n")

print("SNAKE 3:")
snake3 = [[5, 5], [5, 4], [4, 4], [4, 5]]
result = numberOfAvailableDifferentPaths([10, 10], snake3, 4)
print("RESULT: " + str(result) + "\n")
