# Damavis Snake

## Building the Docker Container

Use the Docker build command to build your Docker Image.

```bash
docker build -t snake .
```

## Run script with Docker Container

Use the Docker run command to run your Docker Container.

```bash
docker run -it --rm snake
```
